provider "aws" {
  region = var.aws_region
}

locals {
  s3_origin_id = "S3Origin"
}

# Basic auth function created only if enable_basic_auth is set to yes
resource "aws_cloudfront_function" "basic_auth_function" {
  count   = var.enable_basic_auth == "yes" ? 1 : 0
  name    = "basic-auth"
  runtime = "cloudfront-js-1.0"
  comment = "basic auth function"
  publish = true
  code = templatefile("${path.module}/template/basic_auth_function.tftpl", {
    user = var.auth_username
    pass = var.auth_password
  })
}

# Identity to auth s3
resource "aws_cloudfront_origin_access_identity" "oai" {
  comment = "OAI"
}

# Create cloudFront distribution
resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = var.website_bucket.bucket_regional_domain_name
    origin_id   = local.s3_origin_id
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.oai.cloudfront_access_identity_path
    }
  }

  enabled             = true
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400

    # assocaiated fucntion only if enable_basic_auth is yes 
    dynamic "function_association" {
      for_each = var.enable_basic_auth == "yes" ? [1] : []
      content {
        event_type   = "viewer-request"
        function_arn = element(concat(aws_cloudfront_function.basic_auth_function.*.arn, tolist([""])), 0)
      }
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}
