variable "aws_region" {
  type        = string
  description = "Region to perform the acctions in"
}

variable "website_bucket" {
  type = object({
    bucket_regional_domain_name = string
  })
  description = "website_bucket object"
}

variable "enable_basic_auth" {
  type        = string
  description = "Yes or no to enable/disable basic auth"
}

variable "auth_username" {
  type        = string
  description = "basic auth username"
}

variable "auth_password" {
  type        = string
  description = "basic auth password"
}
