variable "aws_region" {
  type        = string
  description = "Region to perform the acctions in"
}

variable "website_bucket" {
  type = object({
    arn = string
    id  = string
  })
  description = "website_bucket object"
}

variable "oai_id" {
  type        = string
  description = "oai_id used for bucket policy"
}

