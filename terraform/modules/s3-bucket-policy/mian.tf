provider "aws" {
  region = var.aws_region
}

resource "aws_s3_bucket_policy" "public_access" {
  bucket = var.website_bucket.id

  policy = templatefile("${path.module}/template/policy.tftpl",
    {
      bucket-arn = var.website_bucket.arn
      oai-id     = var.oai_id
  })
}
