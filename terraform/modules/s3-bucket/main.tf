provider "aws" {
  region = var.aws_region
}

#Create s3 bucket
resource "aws_s3_bucket" "website_bucket" {
  bucket = var.s3_bucket_name
}

#Set up as static website and index.html as root page
resource "aws_s3_bucket_website_configuration" "website_bucket_config" {
  bucket = aws_s3_bucket.website_bucket.id

  index_document {
    suffix = "index.html"
  }
}


