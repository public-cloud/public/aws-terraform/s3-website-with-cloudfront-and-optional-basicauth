variable "aws_region" {
  type        = string
  description = "Region to perform the acctions in"
}

variable "s3_bucket_name" {
  type        = string
  description = "Name of s3 bucket"
}


