terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">4.46.0"
    }
  }
}


module "s3-bucket" {
  source         = "./modules/s3-bucket"
  aws_region     = var.aws_region
  s3_bucket_name = var.s3_bucket_name
}

module "cloudFront" {
  source            = "./modules/cloudFront"
  aws_region        = var.aws_region
  website_bucket    = module.s3-bucket.website_bucket
  enable_basic_auth = var.enable_basic_auth
  auth_password     = var.auth_password
  auth_username     = var.auth_username
}

module "s3-bucket-policy" {
  source         = "./modules/s3-bucket-policy"
  aws_region     = var.aws_region
  website_bucket = module.s3-bucket.website_bucket
  oai_id         = module.cloudFront.oai_id
}
