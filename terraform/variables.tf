variable "aws_region" {
  type        = string
  description = "home aws region should be us-east-2"
}

variable "s3_bucket_name" {
  type        = string
  description = "Name of s3 bucket"
}

variable "enable_basic_auth" {
  type        = string
  description = "Yes or no to enable/disable basic auth"
}
variable "auth_username" {
  type        = string
  description = "basic auth username"
}

variable "auth_password" {
  type        = string
  description = "basic auth password"
}

