# S3 Website With CloudFront and Optional BasicAuth



## How to use

1. You will need terrafrom installed
2. Run 'aws configure' with the account credentials you want to create this infrastructure in
3. Edit the terrafrom.tfvars file in the terrafrom folder with the information for your usage.  
    &emsp; aws_region - is the region to set up the resources in  
    &emsp; s3_bucket_name - is the name of the s3 bucket and hence must be gloably unique   
    &emsp; enable_basic_auth - enables 'yes' or disables 'no' basic auth for your distribution   
    &emsp; auth_username - is the username you would like to use   
    &emsp; auth_password - is the password you would like to use   
4. You should not need to edit any other file. Once you have it configured to your liking
5. CD into the terraform directory in a terminal
6. Run terraform init, terraform plan, and terraform apply 


